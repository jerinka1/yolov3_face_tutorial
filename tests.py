"""tests for the app"""
import unittest
import argparse

from yolov3 import _main as convert_weights
from face_detection import _main as face_demo


class TestYolov3(unittest.TestCase):
    """tests for the app"""

    def test_convert_weights(self):
        """test convert_weights"""
        convert_weights()


class TestFaceDetection(unittest.TestCase):
    """tests for the app"""

    def test_face_demo(self):
        """test face_demo"""
        args = argparse.Namespace()
        args.image = "samples/test.jpg"
        args.output_dir = "outputs/"
        args.show = False

        boxes = face_demo(args)
        self.assertEqual(len(boxes), 3)
